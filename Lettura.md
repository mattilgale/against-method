---
layout: page
title: Lettura
permalink: /lettura/
---
# Libera Lettura a Festivaletteratura 2022

### Proposta di struttura

- (10-15 min) Domande provocatorie all'inizio. Esempi possibili:
  - C'è una differenza fra scienza e metodo scientifico? [risposta binaria/possibilità di poll istantaneo]
  - Esiste un solo metodo scientifico? [risposta binaria/poll e/o sviluppo in mini-conversazione]
  - Chi è una scienziata o scienziato oggi? che differenza c'è col passato ? e col futuro? [domanda aperta]

- (5-10min) Presentazione di Feyerabend [Mattia]
- (10 min lettura) Passaggi scelti (dalla [preselezione](/against-method/stralci)) [Mattia/Fulvio/Francesco]
- (20 min) Commenti [Mattia/Fulvio/Francesco]
- (20 min) Discussione col pubblico



### Preparazione social

Una settimana prima

- presentazione sulla newsletter
- uso dei canali social per la promozione (telegram/instagram)
