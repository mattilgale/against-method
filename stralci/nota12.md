---
layout: page
title: Nota 12 - Introduzione (1988)
permalink: /stralci/nota12
---

There may, of course, come a time when it will be necessary to give reason a temporary advantage and when it will be wise to defend its rules to the exclusion of everything else. I do not think that we are living in such a time today]
  This was my opinion in 1970 when I wrote the first version of this essay. Times have changed. Considering some tendencies in US education ('politically correct', academic menus, etc.), in philosophy (postmodernism) and in the world at large I think that reason should now be given greater weight not because it is and always was fundamental but because it seems to be needed, in circumstances that occur rather frequently today (but may disappear tomorrow), to create a more humane approach.


##### Traduzione (F.Turci)

Potrà venire ovviamente un tempo in cui sarà necessario concedere alla ragione un temporaneo vantaggio e in cui sarà saggio difenderne le regole a esclusione di ogni altra cosa. Non penso però che noi viviamo già oggi in un tale tempo.]
  Questa era la mia opinione nel 1970, quando scrissi la prima versione di questo saggio. I tempi sono cambiati.Tenendo in considerazione alcune tendenze nel sistema educativo americano (il 'politicamente corretto', i "menu" accademici eccetera), nella filosofia (il postmodernismo) e nel mondo in generale, penso che alla ragione debba esser dato più non tanto perché sia e sia sempre stata fondamentale, ma perché sembra essere necessaria nelle circostanze frequenti oggi (ma possibilmente rare in futuro), al fine di produrre un approccio più umano alle cose.
