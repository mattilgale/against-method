---
layout: page
title: Stralci
permalink: /stralci/
---

Qui seguono gli estratti di lettura.

### Capitoli

- [Capitolo 2 - intero]({% link stralci/capitolo2.md %})
- [Capitolo 3 - selezione]({% link stralci/capitolo3.md %})
- [Capitolo 7 - selezione]({% link stralci/capitolo7.md %})

### Note
- [Nota 12  - Introduzione]({% link stralci/nota12.md %})
- [Nota 13 prima edizione]({% link stralci/nota13.md %})
