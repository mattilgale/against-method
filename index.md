---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---


In questa libera lettura ci interessiamo di **Contro il Metodo** (_Against Method_) di [Paul Feyerabend](https://en.wikipedia.org/wiki/Paul_Feyerabend).

<img  style="float: right;" src="https://cdn-ed.versobooks.com/images/000000/335/9781844674428-frontcover-67d859df7d817fa7e29c0737011cd93a.jpg" alt="cover" width="200"/>


Il testo è un classico dell'epistemologia che rimette in questione il metodo scientifico, attaccandone il _totem_ attraverso una presa di posizione radicale illustrata da una serie di esempi storici, con un focus particolare su Galileo.
